# SuitedConnector Laravel Call Center Models

This repository serves as a Composer package containing commonly-used SuitedConnector models for Laravel.

## Including in an existing project

To include in an existing project:

Add the following in your `composer.json` file

```
    "repositories": [
        {
            "type":"vcs",
            "url": "https://bitbucket.org/suitedconnector/call-center-models.git"
        }
    ],
    "require": {
        "suitedconnector/call-center-models": "dev-master"
    },
```

The models are added to the `App\Models` namespace, and will be usable with:

```
use App\Models\Signup;
```

or:

```
App\Models\Signup::where()
```

etc.