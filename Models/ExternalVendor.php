<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Model;

class ExternalVendor extends Model
{
	protected $table = 'external_vendors';
	protected $connection = 'customer';

	const CREATED_AT = 'created';

	protected $guarded = array('id', 'created');

	public function setUpdatedAt($value) {
		// Do nothing.
	}

	public function getUpdatedAtColumn() {
		return null;
	}

	public function signups() {
		return $this->hasMany('App\Models\Signup');
	}
}
