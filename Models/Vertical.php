<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Vertical extends Model {
	use SoftDeletes;
	protected $guarded = ['id'];
	protected $connection = 'call_center';
	protected $dates = ['created_at', 'updated_at', 'deleted_at'];

	public function campaigns() {
		return $this->hasMany('App\Models\Campaign');
	}
}
