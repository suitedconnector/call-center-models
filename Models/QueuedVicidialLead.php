<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Model;

// Not meant to be used on its own - parent class for LeadToInsert and LeadToDelete
abstract class QueuedVicidialLead extends Model {
	use ColumnEnumerationModelTrait;

	protected $guarded = array('id', 'created_at');
	protected $connection = 'vicidial_queue';

	/**
	 * @param Signup     $signup
	 * @param int $campaign
	 *
	 * @return mixed
	 */
	public static function createFromSignup(Signup $signup, string $dialer_code) {
		$instance = new static();
		$saveData = [
			'campaign_dialer_code' => $dialer_code,
			'signup_id' => $signup->id
		];
		$instanceColumns = $instance->getColumns();
		$signupColumns = $signup->getColumns();
		$commonColumns = array_intersect($instanceColumns, $signupColumns);
		$columnBlacklist = ['id'];
		$commonColumns = array_diff($commonColumns, $columnBlacklist);

		foreach($commonColumns as $column) {
			$saveData[$column] = $signup->$column;
		}

		return self::create($saveData);
	}

	public static function createFromSignupArray(Array $signupArray, string $dialer_code) {
		$instance = new static();
		$saveData = [
			'campaign_dialer_code' => $dialer_code,
			'signup_id' => $signupArray['id']
		];
		$instanceColumns = $instance->getColumns();
		$signupColumns = array_keys($signupArray);
		$commonColumns = array_intersect($instanceColumns, $signupColumns);
		$columnBlacklist = ['id'];
		$commonColumns = array_diff($commonColumns, $columnBlacklist);

		foreach ($commonColumns as $column) {
			$saveData[$column] = $signupArray[$column];
		}

		return self::create($saveData);
	}

}
