<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Buyer extends Model {
	protected $connection = 'customer';

	const CREATED_AT = 'date_created';
	const UPDATED_AT = 'date_updated';

	/**
	 * Indicates if the model should be timestamped.
	 *
	 * @var bool
	 */
	public $timestamps = TRUE;

	/* deny mass assignment to these */
	protected $guarded = ['id', 'date_created', 'date_updated'];

	public function BuyerActivity() {
		return $this->hasMany('App\Models\BuyerActivity');
	}

}
