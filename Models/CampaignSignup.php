<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CampaignSignup extends Model {
	protected $table = 'campaign_signups';
	protected $connection = 'call_center';

	/**
	 * Indicates if the model should be timestamped.
	 *
	 * @var bool
	 */
	public $timestamps = TRUE;

	public function setUpdatedAt($value) {
		// Do nothing.
	}

	public function getUpdatedAtColumn() {
		return NULL;
	}

	/* deny mass assignment to these */
	protected $guarded = ['id', 'created_at', 'updated_at'];

	public function signup() {
		return $this->belongsTo('App\Models\Signup', 'signup_id');
	}

	public function campaign() {
		return $this->belongsTo('App\Models\Campaign', 'campaign_id');
	}
}
