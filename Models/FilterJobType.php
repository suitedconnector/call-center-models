<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class FilterJobType extends Model {
	use SoftDeletes;

	protected $connection = 'call_center';

	protected $dates = ['deleted_at'];

	/**
	 * Indicates if the model should be timestamped.
	 *
	 * @var bool
	 */
	public $timestamps = TRUE;

	/* deny mass assignment to these */
	protected $guarded = ['id', 'created_at', 'updated_at', 'deleted_at'];

	public function campaignFilterSignups() {
		return $this->hasMany('App\Models\CampaignFilterSignup');
	}
}
