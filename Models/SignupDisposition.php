<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SignupDisposition extends Model {
	protected $table = 'signup_dispositions';
	protected $connection = 'call_center';
	protected $dates = ['completed_at'];

	/**
	 * Indicates if the model should be timestamped.
	 *
	 * @var bool
	 */
	public $timestamps = TRUE;

	/* deny mass assignment to these */
	protected $guarded = ['id', 'created_at', 'updated_at'];

	public function campaign() {
		return $this->belongsTo('App\Models\Campaign', 'campaign_id');
	}

	public function disposition() {
		return $this->belongsTo('App\Models\Disposition', 'disposition_id');
	}

	public function signup() {
		return $this->belongsTo('App\Models\Signup', 'signup_id');
	}
}
