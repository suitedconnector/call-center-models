<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Dialer extends Model {
	use SoftDeletes;

	protected $table = 'dialers';
	protected $connection = 'call_center';

	protected $dates = ['created_at', 'updated_at', 'deleted_at'];

	/**
	 * Indicates if the model should be timestamped.
	 *
	 * @var bool
	 */
	public $timestamps = TRUE;

	/* deny mass assignment to these */
	/* for tests we need to specify the dialer id, so removed it from guarded properties - TAC */
	protected $guarded = [ 'created_at', 'updated_at', 'deleted_at'];

	public function Campaigns() {
		return $this->hasMany('App\Models\Campaign', 'dialer_id');
	}

}
