<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CampaignFilterSignup extends Model {
	use SoftDeletes;

	protected $connection = 'call_center';

	protected $dates = ['created_at', 'updated_at', 'deleted_at', 'inserted_at', 'removed_at'];
	protected $casts = ['success' => 'boolean'];

	/**
	 * Indicates if the model should be timestamped.
	 *
	 * @var bool
	 */
	public $timestamps = TRUE;

	/* deny mass assignment to these */
	protected $guarded = ['id', 'created_at', 'updated_at', 'deleted_at'];

	public function filterJobType() {
		return $this->belongsTo('App\Models\FilterJobType');
	}

	public function signup() {
		return $this->belongsTo('App\Models\Signup');
	}

	public function campaign() {
		return $this->belongsTo('App\Models\Campaign');
	}
}
