<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Campaign extends Model {
	use SoftDeletes;

	protected $table = 'campaigns';
	protected $connection = 'call_center';

	protected $dates = ['created_at', 'updated_at', 'deleted_at'];

	/**
	 * Indicates if the model should be timestamped.
	 *
	 * @var bool
	 */
	public $timestamps = TRUE;

	/* deny mass assignment to these */
	protected $guarded = ['id', 'created_at', 'updated_at', 'deleted_at'];

	public function client() {
		return $this->belongsTo('App\Models\Client', 'client_id');
	}

	public function dialer() {
		return $this->belongsTo('App\Models\Dialer', 'dialer_id');
	}

	public function campaignDispositions() {
		return $this->hasMany('App\Models\CampaignDisposition', 'campaign_id');
	}

	public function campaignSignupDispositions() {
		return $this->hasMany('App\Models\CampaignSignupDisposition', 'campaign_id');
	}

	public function campaignSignups() {
		return $this->hasMany('App\Models\CampaignSignup', 'campaign_id');
	}

	public function vertical() {
		return $this->belongsTo('App\Models\Vertical');
	}

	public function campaignFilterSignups() {
		return $this->hasMany('App\Models\CampaignFilterSignups');
	}

	public function exclusiveWaitPeriods() {
		return $this->hasMany('App\Models\ExclusiveWaitPeriod');
	}
}
