<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Model;

class Signup extends Model {
	use ColumnEnumerationModelTrait;

	protected $table = 'signups';
	protected $connection = 'customer';

	/**
	 * Indicates if the model should be timestamped.
	 *
	 * @var bool
	 */
	public $timestamps = FALSE;

	/* deny mass assignment to these */
	protected $guarded = ['id', 'created_at', 'updated_at'];

	/* automatically deserialize these json strings */
	protected $casts = [
		'meta_data' => 'array'
	];

	public function externalVendor() {
		return $this->belongsTo('App\Models\ExternalVendor');
	}

	public function hasExternalVendor() {
		return isset($this->external_vendor_id);
	}

	public function signupDispositions() {
		return $this->hasMany('App\Models\SignupDisposition', 'signup_id');
	}

	public function campaignSignups() {
		return $this->hasMany('App\Models\CampaignSignup', 'signup_id');
	}

	public function campaignFilterSignups() {
		return $this->hasMany('App\Models\CampaignFilterSignup');
	}


	public function getFullNameAttribute() {
		return $this->first_name . ' ' . $this->last_name;
	}

	public function getEmailDomainAttribute() {
		if (filter_var($this->email, FILTER_VALIDATE_EMAIL)) {
			return substr(strrchr($this->email, "@"), 1);
		} else {
			return '';
		}
	}

	public function buyerActivity() {
		return $this->hasMany('App\Models\BuyerActivity');
	}

	public function getHomePhoneAttribute() {
		return isset($this->phone_primary) ? $this->phone_primary : $this->phone_home;
	}
}
