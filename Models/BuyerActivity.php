<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BuyerActivity extends Model {
	protected $table = 'buyeractivity';
	protected $connection = 'customer';

	/**
	 * Indicates if the model should be timestamped.
	 *
	 * @var bool
	 */
	public $timestamps = FALSE;

	/* deny mass assignment to these */
	protected $guarded = ['id'];

	public function signups() {
		return $this->hasMany('App\Models\Signup');
	}

	public function buyer() {
		return $this->belongsTo('App\Models\Buyer');
	}
}
