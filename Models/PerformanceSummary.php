<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PerformanceSummary extends Model {
	protected $table = 'performance_summaries';
	protected $connection = 'call_center';

	/**
	 * Indicates if the model should be timestamped.
	 *
	 * @var bool
	 */
	public $timestamps = TRUE;

	/* deny mass assignment to these */
	protected $guarded = ['id', 'created_at', 'updated_at'];
}
