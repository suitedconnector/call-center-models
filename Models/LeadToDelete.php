<?php

namespace App\Models;

class LeadToDelete extends QueuedVicidialLead
{
	protected $connection = 'vicidial_queue';
	protected $table = 'leads_to_delete';
}
