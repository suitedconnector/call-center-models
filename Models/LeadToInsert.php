<?php

namespace App\Models;

class LeadToInsert extends QueuedVicidialLead
{
	protected $connection = 'vicidial_queue';
	protected $table = 'leads_to_insert';
}
