<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Model;

class ExclusiveWaitPeriod extends Model
{
	public $timestamps = true;
	protected $connection = 'call_center';
	protected $guarded = array('id', 'created_at', 'updated_at');

	public function buyer() {
		return $this->belongsTo('App\Models\Buyer');
	}

	public function campaign() {
		return $this->belongsTo('App\Models\Campaign');
	}
}
