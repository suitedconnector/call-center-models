<?php

namespace App\Models;

use Illuminate\Support\Facades\DB;

trait ColumnEnumerationModelTrait {

	public function getColumns() {
		return DB::connection($this->connection)
			->getSchemaBuilder()
			->getColumnListing($this->getTable());
	}

}
